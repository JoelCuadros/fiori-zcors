jQuery.sap.declare("com.cementos.pacasmayo.zcors.util.Formatter");
jQuery.sap.require("sap.ui.core.format.DateFormat");

com.cementos.pacasmayo.zcors.util.Formatter = {
    
	statusText :  function (value) {
		var bundle = this.getModel("i18n").getResourceBundle();
		return bundle.getText("StatusText" + value, "?");
	},
	
	statusState :  function (value) {
		var map = sap.ui.demo.myFiori.util.Formatter._statusStateMap;
		return (value && map[value]) ? map[value] : "None";
	},
	getfecha : function(fecha){
       	if(fecha != "" && fecha != null){
	       	var valueDate = fecha;
		    var d = new Date(valueDate);
			d.setDate(d.getDate() + 1);
			d = d.toLocaleDateString();
			var parts = d.split('/');
			if(parts[0] < 10) parts[0] = '0' + parts[0];
			if(parts[1] < 10) parts[1] = '0' + parts[1];
			return parts[0] + '.' + parts[1] + '.' + parts[2];
       	}
       	else{
       		return "";
       	}
	},
	quantity :  function (value) {
		try {
			return (value) ? parseFloat(value).toFixed(0) : value;
		} catch (err) {
			return "";
		}
	},
	
	time : function (value)
	{
			var oDateFormat = sap.ui.core.format.DateFormat.getInstance({  
			     source:{pattern:"KKmmss"},  
			     pattern: "KK:mm:ss"}  
			);  
			    value = oDateFormat.parse(value);  
				return oDateFormat.format(new Date(value)); 
	},
	date_time: function(value){
		if(value !== "" && value !== null){
			var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "dd.MM.yyyy hh:mm:ss a"});	
			return oDateFormat.format(value);
		}
       	else{
       		return "";
       	}
	},
	date_formatter: function(fecha){
		if(fecha != "" && fecha != null){
	       	var valueDate = fecha;
		    var d = new Date(valueDate);
			d = d.toLocaleDateString();
			var parts = d.split('/');
			if(parts[0] < 10) parts[0] = '0' + parts[0];
			if(parts[1] < 10) parts[1] = '0' + parts[1];
			return parts[0] + '.' + parts[1] + '.' + parts[2];
       	}
       	else{
       		return "";
       	}
	},
	date_var_formatter: function(fecha, formatter){
		if(fecha !== "" && fecha !== null && fecha !== undefined){
			if(!formatter) formatter = "dd.MM.yyyy";
			var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: formatter});	
			return oDateFormat.format(fecha);
		}
       	else{
       		return "";
       	}	
	},
	datesF1 : function (value) {
		if (value =="00000000"){
		 return ;
		} else{
			var oDateFormat = sap.ui.core.format.DateFormat.getInstance({  
			     source:{pattern:"MM-dd-yyyy"},  
			     pattern: "dd-MM-yyyy"}  
			);  		
	        value = oDateFormat.parse(value);  
			return oDateFormat.format(new Date(value));
		}
	},
	datesF2 : function (value) {
		if (value =="00000000"){
		 return ;
		} else{
			var oDateFormat = sap.ui.core.format.DateFormat.getInstance({  
			     source:{pattern:"MM/dd/yyyy"},  
			     pattern: "dd/MM/yyyy"}  
			);  		
	        value = oDateFormat.parse(value);  
			return oDateFormat.format(new Date(value)); 
		}
	},
	only_date: function(value){
		if(value !== "" && value !== null){
			var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "dd.MM.yyyy"});	
			return oDateFormat.format(value);
		}
       	else{
       		return "";
       	}
	},
	only_time:  function(value){
		if(value !== "" && value !== null){
			var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "hh:mm:ss a"});	
			return oDateFormat.format(value);
		}
       	else{
       		return "";
       	}
	},
	//Función para quitar los 0
	integer: function(num){
		try {
			return (num) ? parseInt(num) : num;
		} catch (err) {
			return "Not-A-Number";
		}
	},
	borrar0izalfanumerico: function(valor){
		if(valor != null && valor != undefined && valor != ""){
			var val = valor;
			for (var i = 0; i < valor.length; i++) {
				if(val.substr(0, 1) == 0){
					val=val.substr(1, val.length-1);
				}
			}
			return val;
		}else{
			return valor;
		}
	},
	//Autor :: Israel Espinoza León
	getSexo: function(value){
		if(value == "M"){
			return "Masculino";
		}else if(value == "F"){
			return "Femenino";
		}else{
			return;
		}
	},
	getEspecialidad: function(value){
		if(value == "001"){
			return "Desarrollo de Software";
		}else if(value == "002"){
			return "Administración Industrial";
		}else{
			return;
		}
	},
	getIdioma: function(value){
		if(value == "01"){
			return "Español";
		}else if(value == "02"){
			return "Inglés";
		}else{
			return;
		}
	},
	redondearDec: function(value, dec){
		if(value != undefined && value != null && value != '' && dec != undefined && dec != null && dec != ''){
			var ceros = '1';
			for (var i = 0; i < dec; i++) {
				ceros += '0';
			}
			return Math.round(value*parseInt(ceros))/parseInt(ceros);
		}else{
			return 0.000
		}
	},
	diasXmes: function(value){
		if(value == "01"){
			return 31;
		}else if(value == "02"){
			return 28;
		}else if(value == "03"){
			return 31;
		}else if(value == "04"){
			return 30;
		}else if(value == "05"){
			return 31;
		}else if(value == "06"){
			return 30;
		}else if(value == "07"){
			return 31;
		}else if(value == "08"){
			return 31;
		}else if(value == "09"){
			return 30;
		}else if(value == "10"){
			return 31;
		}else if(value == "11"){
			return 30;
		}else{
			return 31;
		}
	},
	meses: function(){
		var mes = [{"mes": "01", "mes_desc": "ENERO"}, {"mes": "02", "mes_desc": "FEBRERO"}, {"mes": "03", "mes_desc": "MARZO"}, {"mes": "04", "mes_desc": "ABRIL"}, {"mes": "05", "mes_desc": "MAYO"}, {"mes": "06", "mes_desc": "JUNIO"}, {"mes": "07", "mes_desc": "JULIO"}, {"mes": "08", "mes_desc": "AGOSTO"}, {"mes": "09", "mes_desc": "SETIEMBRE"}, {"mes": "10", "mes_desc": "OCTUBRE"}, {"mes": "11", "mes_desc": "NOVIEMBRE"}, {"mes": "12", "mes_desc": "DICIEMBRE"}];

		return mes;
	},
	anioIA: function(anio){
		var fechaA = new Date(),
			anioA = fechaA.getFullYear(),
			anioArray = [],
			diferencia = new Array(parseInt(anioA) - parseInt(anio));


		anioArray.push({"anio": anio, "anio_desc": anio});

		$.each(diferencia, function(k, v){
			anio = parseInt(anio + 1);
			anioArray.push({"anio": anio.toString(), "anio_desc": anio.toString()});
		});

		return anioArray;
	},
	status: function(value){
		if(value == "G"){
			return "Generado";
		}else if(value == "P"){
			return "Aprobado";
		}else if(value == "A"){
			return "Anulado";
		}else{
			return;
		}
	},
	statusSap: function(value){
		if(value == "S"){
			return "Enviado";
		}else if(value == "N"){
			return "Pendiente";
		}else{
			return;
		}
	},
	monto: function(value){
		if(value != null && value != undefined){
			return parseFloat(value).toFixed(3).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");	
		}else{
			return "";
		}
   	},
   	descrmes: function(value){
		if(value == "01"){
			return "ENERO";
		}else if(value == "02"){
			return "FEBRERO";
		}else if(value == "03"){
			return "MARZO";
		}else if(value == "04"){
			return "ABRIL";
		}else if(value == "05"){
			return "MAYO";
		}else if(value == "06"){
			return "JUNIO";
		}else if(value == "07"){
			return "JULIO";
		}else if(value == "08"){
			return "AGOSTO";
		}else if(value == "09"){
			return "SETIEMBRE";
		}else if(value == "10"){
			return "OCTUBRE";
		}else if(value == "11"){
			return "NOVIEMBRE";
		}else{
			return "DICIEMBRE";
		}
	},
	codigoSapMaterial: function(valor){
		var resultado = "";
 		if(valor != undefined && valor != null){
 			resultado = valor.substr(10, 3) + "-" + valor.substr(13);
 		}

 		return resultado;
	},
	 //Función para quitar los 0
	 integer: function(num){
        try {
            return (num) ? parseInt(num) : num;
        } catch (err) {
            return "Not-A-Number";
        }
	},

	diferencia: function(valor){
		var resultado = "";
		var validacionGuion = "";
 		if(valor != undefined && valor != null){
			validacionGuion = valor.substr(valor.length - 1);
			if(validacionGuion == "-"){
				resultado = valor.substr(0, valor.length - 1);
				resultado = "-" + resultado;
			}else{
				resultado = valor;
			}
 		}

 		return resultado;
    },

	devolverNumero: function(valor){
		try {
            return valor==undefined?"0.000 ":valor.toString();
        } catch (err) {
            return "0.000 ";
        }
   
    }
};